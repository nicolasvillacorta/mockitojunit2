package com.probando.business;


import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.probando.data.api.TodoService;
import com.probando.data.api.TodoServiceStub;

public class TodoBusinessImplStubTest {

	@Test
	public void testretrieveTodosRelatedToSpring_usingAStub() {
		TodoService todoServiceStub = new TodoServiceStub();
		TodoBusinessImpl todoBusinessImpl = 
				new TodoBusinessImpl(todoServiceStub);
		List<String> filteredTodos = todoBusinessImpl
				.retrieveTodosRelatedToSpring("Dummy");
		assertEquals(2, filteredTodos.size());
	}
	



}
