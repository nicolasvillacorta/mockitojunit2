package com.probando.data.api;

import java.util.Arrays;
import java.util.List;

public class TodoServiceStub implements TodoService{
	//Dynamic Conditions
	//Service Definition
	
	public List<String> retrieveTodos(String user) {
		
		return Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
	}
	
}
